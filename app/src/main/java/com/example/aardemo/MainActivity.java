package com.example.aardemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.libmodule.RegisterFragment;
import com.example.registerform.RegistrationFormFragment;

public class MainActivity extends AppCompatActivity implements RegisterFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.frameContainer, RegisterFragment.newInstance(), getString(R.string.register_fragment)).commitAllowingStateLoss();
        }
    }

    @Override
    public void onFragmentInteraction() {
        getSupportFragmentManager().beginTransaction().add(R.id.frameContainer, new RegistrationFormFragment(), getString(R.string.register_form_fragment)).addToBackStack(getString(R.string.register_fragment)).commitAllowingStateLoss();
    }
}
